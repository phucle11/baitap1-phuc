const tasks = (state=[], action) => {
    switch(action.type) {
        case 'ADD_TASK':
            const now = new Date()
            return [
                ...state,
                {
                    id: action.id,
                    task: action.task,
                    completed: false,
                    time: `${now.getDate()}/${now.getMonth()}/${now.getFullYear()}. 
                    ${now.getHours()}:${now.getMinutes()}`
                }
            ]
        case 'CLEAR_TASK':
            return []
        case 'DONE_TASK':
            const nowComplete = new Date()
            return state.map(task => 
                (task.id === action.id)? {
                    ...task, 
                    text: 'completed',
                    completed: true,
                    time: `${nowComplete.getDate()}/${nowComplete.getMonth()}/${nowComplete.getFullYear()}. 
                    ${nowComplete.getHours()}:${nowComplete.getMinutes()}`
                }: task
            )
        case 'EDIT_TASK':
            const nowEdit = new Date()
            let editTask = prompt("edit your task")
            if (editTask === null)
            return state
            return state.map(task => 
                (task.id === action.id)? {
                    ...task,
                    task: editTask,
                    time: `${nowEdit.getDate()}/${nowEdit.getMonth()}/${nowEdit.getFullYear()}. 
                    ${nowEdit.getHours()}:${nowEdit.getMinutes()}`
                }: task
            )
        case 'DELETE_TASK':
            return state.filter(task => 
                (task.id !== action.id)
            )
        default:
            return state
    }
}

export default tasks