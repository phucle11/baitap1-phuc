let nextTaskId = 0

export const addTask = (task) => ({
    type: 'ADD_TASK',
    id: nextTaskId++,
    task
})

export const editTask = (id) => ({
    type: 'EDIT_TASK',
    id
})

export const doneTask = (id) => ({
    type: 'DONE_TASK',
    id
})

export const deleteTask = (id) => ({
    type: 'DELETE_TASK',
    id
})

export const clearTask = () => ({
    type: 'CLEAR_TASK'
})

export const statusFilters = {
    COMPLETED: 'COMPLETED',
    ACTIVE: 'ACTIVE'
}