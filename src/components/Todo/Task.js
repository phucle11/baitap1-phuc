import React from 'react'

const Task =  ({task, time, onClickDone, onClickEdit, onClickDelete}) => (
    <li>
        <div className="task">
            {task}
        </div>
        <div className="info">
            <span>{time}</span>
            <button type="button" className="btn btn-complete" onClick={onClickDone}>Complete</button>
            <button type="button" className="btn" onClick={onClickEdit}>Edit</button>
            <button type="button" className="btn btn-delete" onClick={onClickDelete}>Delete</button>
        </div>
    </li>
)

export default Task