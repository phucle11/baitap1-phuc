import React from 'react'

const CompletedTask =  ({task, time, text, onClickDelete}) => (
    <li className="li-completed">
        <div className="task">
            {task}
        </div>
        <div className="info">
            <span>{text} on {time}</span>
            <button type="button" className="btn btn-delete" onClick={onClickDelete}>Delete</button>
        </div>
    </li>
)

export default CompletedTask