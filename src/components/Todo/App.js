import React from 'react'
import AddTask from '../../containers/Todo/AddTask'
import TaskList from '../../containers/Todo/TaskList'
import '../../App.css'

function App() {
  return (
    <div className="App">
      <p id="title">toDo List</p>
      <AddTask />
      <TaskList />
    </div>
  )
}

export default App
