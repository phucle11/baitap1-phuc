import React from 'react'
import { connect } from 'react-redux'
import Task from '../../components/Todo/Task'
import CompletedTask from '../../components/Todo/CompletedTask'
import { doneTask, editTask, deleteTask, statusFilters } from '../../redux/actions'

function getTaskList(tasks, taskStatus) {
    switch(taskStatus) {
        case 'COMPLETED':
            return tasks.filter(task => task.completed)
        case 'ACTIVE':
            return tasks.filter(task => !task.completed)
        default:
            return tasks
    }
}

const TaskList = ({completedTasks, activeTasks, doneTask, editTask, deleteTask}) => (
    <div>
        <ul><span className="task-type">Task-to-do <span className="task-num">{activeTasks.length}</span></span>
            {activeTasks.map(task =>
            <Task 
            key={task.id}
            {...task}
            onClickDone={() => doneTask(task.id)}
            onClickEdit={() => editTask(task.id)}
            onClickDelete={() => deleteTask(task.id)}
            />)}
        </ul>
        <ul><span className="task-type task-type-completed">Completed-task <span className="task-num task-num-completed">{completedTasks.length}</span></span>
            {completedTasks.map(task =>
            <CompletedTask 
            key={task.id}
            {...task}
            onClickDelete={() => deleteTask(task.id)}
            />)}
        </ul>
    </div>
)

const mapStateToProps = (state) => ({
    completedTasks: getTaskList(state.tasks, statusFilters.COMPLETED),
    activeTasks: getTaskList(state.tasks, statusFilters.ACTIVE)
})

const mapDispatchToProps = (dispatch) => ({
    doneTask: id => dispatch(doneTask(id)),
    editTask: id => dispatch(editTask(id)),
    deleteTask: id => dispatch(deleteTask(id))
})

export default connect(mapStateToProps, mapDispatchToProps)(TaskList)
