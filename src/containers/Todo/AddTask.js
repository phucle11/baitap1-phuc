import React from 'react'
import { connect } from 'react-redux'
import { addTask, clearTask } from '../../redux/actions'

const AddTask = ({addTask, clearTask}) => {
    let input 
    const onAddTask = (e) => {
        e.preventDefault()
        if(input.value.trim() === '')
            return
        addTask(input.value)
        input.value = ''
    }
    const onClearTask = (e) => {
        e.preventDefault()
        clearTask()
    }
    return (
        <div>
            <form id="form">
                <input placeholder="Add new task..." ref={node => input = node}/>
                <span id="button-wrap"><button type="submit" className="btn" onClick={onAddTask}>Add</button>
                <button type="button" className="btn" onClick={onClearTask}>Clear</button></span>
            </form>
        </div>
    )
}

const mapDispatchToProps = (dispatch) => ({
    addTask: task => dispatch(addTask(task)),
    clearTask: () => dispatch(clearTask())
})

export default connect(null, mapDispatchToProps)(AddTask)